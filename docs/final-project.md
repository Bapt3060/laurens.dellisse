###### 08 octobre 2020
# Mise en bouche
## Uni Sign Pen 
### Design et fabrication
### Contexte de création
### Influence culturelle

Le Japon porte un très grand intérêt à l’écriture depuis le 5 ème siècle lors de son développement. Celle-ci se composant d’idéogrammes en partie inspirés de Chine et de deux types d’écritures propres au Japon, elle est intéressante d’un point de vue de l’art qui se cache derrière la représentation ainsi que la signification des symboles. 

Au début, plus pratiquée par les nobles et ensuite par les samouraïs, la représentation de ces symboles, la calligraphie japonaise qui dans sa traduction *shodo* signifiant “la voie de l’écriture” renvoie à la religion bouddhiste et à un aspect spirituel de l’art d’écrire à la main qui représente davantage un art de vivre qui maintient l'esprit et le corps qu’un art purement esthétique. (Certains moines l’utilisent pour de la méditation)

Aujourd’hui cet art est intégré dès le plus jeune âge dans les écoles japonaises, et malgré la difficulté de cet apprentissage, le taux d'illettrisme au Japon est infiniment peu élevé et c’est spécifiquement l’emploi d’outils manuels qui justifierait cela selon les puristes japonais, le passage du dessin à la main apporte beaucoup à la compréhension des symboles.

Pour cet art, 4 outils primordiaux sont nécessaires (4 trésors du calligraphe):

![influence culturelle](docs/images/influence_culturelle_Sign_Pen.png)

La préparation de l’encre est une étape nécessaire à la préparation de l’esprit, néanmoins de nos jours, l’emploi de stylo ou du feutre devient de plus en plus répandu pour la calligraphie. 

Ces outils représentent un format portable plus compact de plusieurs éléments, ils peuvent être transportés dans une poche et représentent un gain de temps de préparation vu qu’ils sont prêts à l’emploi directement.

![influence culturelle 2](docs/images/influence_culturelle_2.png)

Outre l’art de la calligraphie, les mangas représentent un secteur très prisé notamment à l’étranger, la pratique de cet art nécessite l’emploi de divers instruments d’écriture tel que les feutres. Le Japon excelle dans les stylos et feutres avec une spécialisation dans la pointe fine permettant aussi divers épaisseurs et arrondis.

Le sign pen semble reprendre les caractéristiques de base nécessaire à la calligraphie japonaise, de l’encre et de l’eau. Cela permet de rendre l’encre résistante à l’eau et donc au temps et donc inaltérable. Néanmoins elle ne possède pas une brosse flexible ce qui lui permet donc de créer peu de variantes dans les épaisseurs mais celle-ci ne laisse pas l’encre transpercer le papier.

### Test et avis 

Nous avons testé le feutre sign pen sous différents angles. Celui-ci permet de jouer avec les épaisseurs de traits en fonction de son inclinaison. De cette manière, nous pouvons tracer des lignes relativement fines ainsi que des traits plus marque.
Il est important de noter que les différents tests ont été réalisé sur du papier 300g à grains fort. Les traits sont donc peu régulidés et nous pouvons voir que l’écoulement de l’encre n’est parfois pas suffisant. 

![Test Papier 300g](docs/images/test_papier_300g.png)

En revanche, lorsque le marqueur est utilisé sur une surface plus lisse, plus régulière, ici un support cartonné brillant, les traits sont beaucoup plus précis et les lignes plus régulières. En effet le carton blanc absorbe moins d’encre par rapport au papier 300 grammes. 

![Test Carton lisse](docs/images/test_carton_blanc.png)

Nous l’avons également soumis à un test de dessin de perspective. Ce dessin a été réalisé uniquement avec le Sign Pen Uni. Il s’agit donc d’un feutre polyvalent utile pour tracer des lignes franches et à la fois des détails beaucoup plus fins.

![Test dessin perspective](docs/images/test_sketch_sign_pen.png)

De manière générale, nous constatons après test que le sign pen uni est un bon outil de dessin, principalement lorsqu’il s’agit de tracer des lignes et des écritures. Il ne se prête pas aux grands aplats ainsi qu'a toutes les surfaces de dessin en raison du flux d'encre limité, même si cela peut etre un un atout pour un effet de style.

###### 17 octobre 2020

## LCP/Maarten Van severen
### Contexte de création
### Influence
### Maarten Van Severen
### Design et Innovation
#### Structure
#### transparence et Couleur
#### legereté










# Final Project


This week I worked on defining my final project idea and started to getting used to the documentation process.

## Research

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

> "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

## 2D and 3D Modeling

Add here your modeling and design.


## Some other section

This is an updated text.



## Materials

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | Material one    |  22.00 $| http://amazon.com/test   |    Order many    |
| 1   | Material two    |  22.00 $| http://amazon.com/test   |        |
| 1   | Material three  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material five   |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eight  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material twelve |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eleven |  22.00 $| http://amazon.com/test   |        |

## Useful links

- [Jekyll](http://jekyll.org)
- [Google](http://google.com)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## Gallery

![](../images/sample-photo.jpg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/jjNgJFemlC4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
