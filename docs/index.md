###### 13 octobre 2020
# A propos de moi 

![Laurens](docs/images/pp.png)
docs/images/pp.png

Bonjour ! Je m’appelle Laurens Dellisse, je suis étudiant à la faculté d'architecture la Cambre - Horta depuis maintenant un peu plus de quatre ans. Je termine donc mes études d'archi cette année. Je m'interesse à toutes échelles d'architecture et de conception, du travail de l'urbaniste au travail du designer. J'éprouve aussi un très grand intérêt pour la fabrication "manuelle" et l'expérimentation.

## Background 

Je suis né à Namur et y ai également grandi. Au milieu des champs et des vaches j'ai toujours aimé bricoler et bidouiller. Cette passion du bricolage, je l'ai développé auprès d'un artiste-artisan, Gérard Havelange, qui habitait dans ma rue. Il m'a initié à l'art de sculpter le bois il y a maintenant quelques années ainsi qu'à la notion de réutilisation, de réemplois et de détournement d'objet.

## Travail précédent

J'ai déjà eu la chance de suivre le cours d'architecture et design ainsi que certaines formations fablab l'année passée (2019-2020) pour l'utilisation de découpeuse laser, d'imprimante 3 D, de découpeuse vinyle et de CNC. Je pense donc être déjà assez familiarisé avec la fabrication d'objets via les techniques numériques.

J'ai également travaillé dans un bureau d'architecture namurois, Urban Architectes (https://urbanarchitectes.be), ou je me suis penché sur la réalisation d'une maquette entièrement réalisée avec l'équipement du fablab TRAKK (https://www.trakk.be). La construction de ce modèle a fait appel à l'utilisation de différentes machines numériques (découpeuse laser, imprimante 3 D, CNC)

### Marble Chair

![MarbleChair](docs/images/Marble_chair_démarche.png)

Ce projet de mobilier d’intérieur et/ou d’extérieur consiste en un essai à la question : «comment s’assied-on aujourd’hui, dans un contexte ou écologie et l’autoconstruction ne cesse de prendre de l’intérêt». Cet élément de mobilier consiste donc à rendre l’utilisateur, constructeur du projet. En effet, les éléments anglés constituant l’assise et le dossier de la chaise sont moulés directement dans les pièces structurelles formant les pieds du meuble. Il s’agit donc d’une chaise moulée ou le moule n’est pas laissé dans l’oubli après la création de l’objet. Les moules gardent une utilité forte en étant directement intégrés dans l’objet. L’approche écologique de l’objet est assez directe. Divers prototypes d’assises ont été réalisés avec différentes matières destinées à être jetées (papier, carton, tissu, plastique et plastique thermodurcissable. Ces matières sont donc moulées puis assemblées avec la structure. l’objet entre dans un cycle car lorsqu’une pièce est abimée, elle peut être remoulée directement. 

### Maquette quartier des confluents 

![Laurens](docs/images/maquette_urban_architectes.png)
cette maquette conçue pour le développement urbanistique et la vente de logement en bord de Meuse fut le fruit d'une modélisation complexe. En effet, la taille imposante de l'objet nous a obligé à concevoir le modèle à l'aide d'objet dynamique, interagissant entre eux. cela a permis de mieux travailler les différents emboitements ainsi que les nombreux raccords entre la topographie et les bâtiments.

###### 17 octobre 2020
# ADAM (Design Museum of Brussels)

Lors de la visite du musée, je dois avouer que je ne me sentais pas spécialement stimulé par les différents objets qui y étaient présentés. En effet, je ne suis pas un grand fan des formes "blob", moulée que l'on retrouve dans le mobilié des années soixante, septantes. Cependant, une fois les apparences mises de coté, et que l'on s'intéresse aux points phares des innovations, certaines pièces semblaient se démarquer.

![ADAM collection](docs/images/ADAM_museum_collection.png)

## LCP / Maarten Van Severen

![](docs/images/video-1603031935.mp4)

Mon intérêt s'est porté sur une chaise basse originale. Certes moins détaillé et d'apparence moins modelée que le reste de la collection du musée, mais tout autant ingénieuse et innovante. La chaise paraissait effectivement beaucoup plus simple et épurée que les autres pièces. C'est cette apparence trompeuse qui m'a attiré vers cet objet. Croire qu'il s'agit d'une assure classique en compression alors qu'il s'agit d'un système de traction et compression amenant une certaine élasticité et donc un sentiment de lévitation. Croire que la chaise est monochrome, alors que celle-ci crée un jeu de couleurs subtile de par sa translucidité. Croire que l'objet ne résistera pas sa similarité avec le verre alors que le plastique acrylique qui la compose a une résistance supérieur à celle du verre. Croire que les courbes qui la composent est simplement esthétique alors que celles-ci régulent la flexion de l'assise. Croire qu'un objet minimaliste est trop simple alors qu il est complexe.

![LCP 2002](docs/images/LCP_1.png)


